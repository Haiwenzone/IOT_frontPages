﻿/**
 * @license
 * (c) 2009-2013 Michael Leibman
 * michael{dot}leibman{at}gmail{dot}com
 * http://github.com/mleibman/slickgrid
 *
 * Distributed under MIT license.
 * All rights reserved.
 *
 * SlickGrid v2.2
 *
 * NOTES:
 *     Cell/row DOM manipulations are done directly bypassing jQuery's DOM manipulation methods.
 *     This increases the speed dramatically, but can only be done safely because there are no event handlers
 *     or data associated with any cell/row DOM nodes.  Cell editors must make sure they implement .destroy()
 *     and do proper cleanup.
 */
if (typeof jQuery == "undefined") throw"SlickGrid requires jquery module to be loaded";
if (!jQuery.fn.drag) throw"SlickGrid requires jquery.event.drag module to be loaded";
if (typeof Slick == "undefined") throw"slick.core.js not loaded";
(function (n) {
    function r(r, u, f, e) {
        function is() {
            var s, u;
            if (y = n(r), y.length < 1) throw new Error("SlickGrid requires a valid container, " + r + " does not exist in the DOM.");
            if (e.scaleHeightToDockContentParent) {
                for ($dockContentParent = y; !$dockContentParent.hasClass("dock-content");) $dockContentParent = $dockContentParent.parent();
                var c = $dockContentParent.height(),
                    l = y[0].getBoundingClientRect().top - $dockContentParent[0].getBoundingClientRect().top, h = c - l;
                y.height(h < 200 ? 200 : h)
            }
            for (i = i || cs(), t = t || ss(), e = n.extend({}, ts, e), le(), hu.width = e.defaultColumnWidth, fr = {}, s = 0; s < f.length; s++) u = f[s] = n.extend({}, hu, f[s]), fr[u.id] = s, u.minWidth && u.width < u.minWidth && (u.width = u.minWidth), u.maxWidth && u.width > u.maxWidth && (u.width = u.maxWidth);
            if (e.enableColumnReorder && !n.fn.sortable) throw new Error("SlickGrid's 'enableColumnReorder = true' option requires jquery-ui.sortable module to be loaded");
            vr = {commitCurrentEdit: bl, cancelCurrentEdit: kl};
            y.empty().css("overflow", "hidden").css("outline", 0).attr("role", "grid").attr("tabindex", 0).addClass(nt).addClass("ui-widget");
            e.gridLabel && y.attr("aria-label", e.gridLabel);
            /relative|absolute|fixed/.test(y.css("position")) || y.css("position", "relative");
            rr = n("<div tabIndex='0' class='slick-hideFocus' role='presentation'><\/div>").appendTo(y);
            pi = n("<div class='slick-header ui-state-default' style='overflow:hidden;position:relative;' role='presentation' />").appendTo(y);
            p = n("<div class='slick-header-columns' style='left:-1000px' role='row' />").appendTo(pi);
            p.width(du());
            yt = n("<div class='slick-headerrow ui-state-default' style='overflow:hidden;position:relative;' />").appendTo(y);
            ei = n("<div class='slick-headerrow-columns' />").appendTo(yt);
            vf = n("<div style='display:block;height:1px;position:absolute;top:0;left:0;'><\/div>").css("width", ie() + t.width + "px").appendTo(yt);
            oi = n("<div class='slick-top-panel-scroller ui-state-default' style='overflow:hidden;position:relative;' />").appendTo(y);
            yf = n("<div class='slick-top-panel' style='width:10000px' />").appendTo(oi);
            e.showTopPanel || oi.hide();
            e.showHeaderRow || yt.hide();
            g = n("<div class='slick-viewport' style='width:100%;overflow:auto;outline:0;position:relative;;'>").appendTo(y);
            g.css("overflow-y", e.autoHeight ? "hidden" : "auto");
            tt = n("<div class='grid-canvas' role='presentation' />").appendTo(g);
            ar = rr.clone().appendTo(y);
            e.explicitInitialization || ne();
            o.onSort.subscribe(rs.bind(this))
        }

        function rs(n, t) {
            wdpFireAriaMessage(t.sortAsc ? "Ascending" : "Descending")
        }

        function ne() {
            vt || (vt = !0, et = parseFloat(n.css(y[0], "width", !0)), ks(), hs(p), uf(), re(), ws(), ee(), gt(), ls(), y.bind("resize.slickgrid", gt), g.bind("scroll", hr), pi.bind("contextmenu", kc).bind("click", dc).delegate(".slick-header-column", "mouseenter", wc).delegate(".slick-header-column", "mouseleave", bc), yt.bind("scroll", nc), rr.bind("keydown", ac), ar.bind("keydown", vc), tt.bind("keydown", cc).bind("click", to).bind("dblclick", pc).bind("contextmenu", yc).bind("draginit", ec).bind("dragstart", {distance: 3}, oc).bind("drag", sc).bind("dragend", hc).delegate(".slick-cell", "mouseenter", gc).delegate(".slick-cell", "mouseleave", nl), p.bind("keydown", lc), navigator.userAgent.toLowerCase().match(/webkit/) && navigator.userAgent.toLowerCase().match(/macintosh/) && tt.bind("mousewheel", fc))
        }

        function us(n) {
            ti.unshift(n);
            n.init(o)
        }

        function te(n) {
            for (var t = ti.length; t >= 0; t--) if (ti[t] === n) {
                ti[t].destroy && ti[t].destroy();
                ti.splice(t, 1);
                break
            }
        }

        function fs(n) {
            ot && (ot.onSelectedRangesChanged.unsubscribe(he), ot.destroy && ot.destroy());
            ot = n;
            ot && (ot.init(o), ot.onSelectedRangesChanged.subscribe(he))
        }

        function es() {
            return ot
        }

        function os() {
            return tt[0]
        }

        function ss() {
            var t = n("<div style='position:absolute; top:-10000px; left:-10000px; width:100px; height:100px; overflow:scroll;'><\/div>").appendTo("body"),
                i = {width: t.width() - t[0].clientWidth, height: t.height() - t[0].clientHeight};
            return t.remove(), i
        }

        function du() {
            for (var r, n = 0, i = 0, u = f.length; i < u; i++) r = f[i].width, n += r;
            return n += t.width, Math.max(n, et) + 1e3
        }

        function ie() {
            for (var r = ki ? et - t.width : et, n = 0, i = f.length; i--;) n += f[i].width;
            return e.fullWidthRows ? Math.max(n, r) : n
        }

        function gu(n) {
            var i = pt;
            pt = ie();
            pt != i && (tt.width(pt), ei.width(pt), p.width(du()), au = pt > et - t.width);
            vf.width(pt + (ki ? t.width : 0));
            (pt != i || n) && rf()
        }

        function hs(n) {
            n && n.jquery && n.attr("unselectable", "on").css("MozUserSelect", "none").bind("selectstart.ui", function () {
                return !1
            })
        }

        function cs() {
            for (var i = 1e6, u = navigator.userAgent.toLowerCase().match(/firefox/) ? 6e6 : 1e9, r = n("<div style='display:none' />").appendTo(document.body), t; ;) if (t = i * 2, r.css("height", t), t > u || r.height() !== t) break; else i = t;
            return r.remove(), i
        }

        function ls() {
            for (var t = tt[0], i; (t = t.parentNode) != document.body && t != null;) (t == g[0] || t.scrollWidth != t.clientWidth || t.scrollHeight != t.clientHeight) && (i = n(t), si = si ? si.add(i) : i, i.bind("scroll." + nt, so))
        }

        function as() {
            si && (si.unbind("scroll." + nt), si = null)
        }

        function vs(n, t, i) {
            var r, e, u;
            vt && (r = kt(n), r != null) && (e = f[r], u = p.children().eq(r), u && (t !== undefined && (f[r].name = t), i !== undefined && (f[r].toolTip = i), h(o.onBeforeHeaderCellDestroy, {
                node: u[0],
                column: e
            }), u.attr("title", i || "").children().eq(0).html(t), h(o.onHeaderCellRendered, {node: u[0], column: e})))
        }

        function ys() {
            return ei[0]
        }

        function ps(n) {
            var i = kt(n), t = ei.children().eq(i);
            return t && t[0]
        }

        function re() {
            function s() {
                n(this).addClass("ui-state-hover")
            }

            function c() {
                n(this).removeClass("ui-state-hover")
            }

            var i, t, r, u;
            for (p.find(".slick-header-column").each(function () {
                var t = n(this).data("column");
                t && h(o.onBeforeHeaderCellDestroy, {node: this, column: t})
            }), p.empty(), p.width(du()), ei.find(".slick-headerrow-column").each(function () {
                var t = n(this).data("column");
                t && h(o.onBeforeHeaderRowCellDestroy, {node: this, column: t})
            }), ei.empty(), i = 0; i < f.length; i++) {
                if (t = f[i], r = n("<div class='ui-state-default slick-header-column' />").html("<span class='slick-column-name'>" + t.name + "<\/span>").attr("id", "" + nt + t.id).attr("title", t.toolTip || "").attr("role", "columnheader").data("column", t).addClass(t.headerCssClass || "").appendTo(p).width(t.width - hi), e.enableColumnReorder || t.sortable) r.on("mouseenter", s).on("mouseleave", c);
                (e.rowHeaderName && e.rowHeaderName === t.name || e.rowHeaderId && e.rowHeaderId === t.id) && (e.rowHeaderColumn = i);
                t.sortable && (r.addClass("slick-header-sortable"), r.append("<span class='slick-sort-indicator' />"), r.attr("tabindex", 0));
                h(o.onHeaderCellRendered, {node: r[0], column: t});
                e.showHeaderRow && (u = n("<div class='ui-state-default slick-headerrow-column l" + i + " r" + i + "'><\/div>").data("column", t).appendTo(ei), h(o.onHeaderRowCellRendered, {
                    node: u[0],
                    column: t
                }))
            }
            kr(rt);
            fe();
            e.enableColumnReorder && bs()
        }

        function ws() {
            p.click(ue)
        }

        function ue(t) {
            var s, r, i, u;
            if ((t.metaKey = t.metaKey || t.ctrlKey, !n(t.target).hasClass("slick-resizable-handle")) && (s = n(t.target).closest(".slick-header-column"), s.length) && (r = s.data("column"), r.sortable)) {
                if (!k().commitCurrentEdit()) return;
                for (i = null, u = 0; u < rt.length; u++) if (rt[u].columnId == r.id) {
                    i = rt[u];
                    i.sortAsc = !i.sortAsc;
                    break
                }
                t.metaKey && e.multiColumnSort ? i && rt.splice(u, 1) : ((t.shiftKey || t.metaKey) && e.multiColumnSort || (rt = []), i ? rt.length == 0 && rt.push(i) : (i = {
                    columnId: r.id,
                    sortAsc: r.defaultSortAsc
                }, rt.push(i)));
                kr(rt);
                e.multiColumnSort ? h(o.onSort, {
                    multiColumnSort: !0, sortCols: n.map(rt, function (n) {
                        return {sortCol: f[kt(n.columnId)], sortAsc: n.sortAsc}
                    })
                }, t) : h(o.onSort, {multiColumnSort: !1, sortCol: r, sortAsc: i.sortAsc}, t)
            }
        }

        function bs() {
            p.filter(":ui-sortable").sortable("destroy");
            p.sortable({
                containment: "parent",
                distance: 3,
                axis: "x",
                cursor: "default",
                tolerance: "intersection",
                helper: "clone",
                placeholder: "slick-sortable-placeholder ui-state-default slick-header-column",
                start: function (t, i) {
                    i.placeholder.width(i.helper.outerWidth() - hi);
                    n(i.helper).addClass("slick-header-column-active")
                },
                beforeStop: function (t, i) {
                    n(i.helper).removeClass("slick-header-column-active")
                },
                stop: function (t) {
                    var r, u, i;
                    if (!k().commitCurrentEdit()) {
                        n(this).sortable("cancel");
                        return
                    }
                    for (r = p.sortable("toArray"), u = [], i = 0; i < r.length; i++) u.push(f[kt(r[i].replace(nt, ""))]);
                    ce(u);
                    h(o.onColumnsReordered, {});
                    t.stopPropagation();
                    fe()
                }
            })
        }

        function fe() {
            var v, i, t, u, r, c, l, s, a;
            (r = p.children(), r.find(".slick-resizable-handle").remove(), r.each(function (n) {
                f[n].resizable && (s === undefined && (s = n), a = n)
            }), s !== undefined) && r.each(function (y, p) {
                y < s || e.forceFitColumns && y >= a || (v = n(p), n("<div class='slick-resizable-handle' />").appendTo(p).bind("dragstart", function (o) {
                    var a, s, v, h;
                    if (!k().commitCurrentEdit()) return !1;
                    if (u = o.pageX, n(this).parent().addClass("slick-header-column-active"), a = null, s = null, r.each(function (t, i) {
                        f[t].previousWidth = n(i).outerWidth()
                    }), e.forceFitColumns) for (a = 0, s = 0, i = y + 1; i < r.length; i++) t = f[i], t.resizable && (s !== null && (t.maxWidth ? s += t.maxWidth - t.previousWidth : s = null), a += t.previousWidth - Math.max(t.minWidth || 0, ni));
                    for (v = 0, h = 0, i = 0; i <= y; i++) t = f[i], t.resizable && (h !== null && (t.maxWidth ? h += t.maxWidth - t.previousWidth : h = null), v += t.previousWidth - Math.max(t.minWidth || 0, ni));
                    a === null && (a = 1e5);
                    v === null && (v = 1e5);
                    s === null && (s = 1e5);
                    h === null && (h = 1e5);
                    l = u + Math.min(a, h);
                    c = u - Math.min(v, s)
                }).bind("drag", function (n) {
                    var s, h = Math.min(l, Math.max(c, n.pageX)) - u, o;
                    if (h < 0) {
                        for (o = h, i = y; i >= 0; i--) t = f[i], t.resizable && (s = Math.max(t.minWidth || 0, ni), o && t.previousWidth + o < s ? (o += t.previousWidth - s, t.width = s) : (t.width = t.previousWidth + o, o = 0));
                        if (e.forceFitColumns) for (o = -h, i = y + 1; i < r.length; i++) t = f[i], t.resizable && (o && t.maxWidth && t.maxWidth - t.previousWidth < o ? (o -= t.maxWidth - t.previousWidth, t.width = t.maxWidth) : (t.width = t.previousWidth + o, o = 0))
                    } else {
                        for (o = h, i = y; i >= 0; i--) t = f[i], t.resizable && (o && t.maxWidth && t.maxWidth - t.previousWidth < o ? (o -= t.maxWidth - t.previousWidth, t.width = t.maxWidth) : (t.width = t.previousWidth + o, o = 0));
                        if (e.forceFitColumns) for (o = -h, i = y + 1; i < r.length; i++) t = f[i], t.resizable && (s = Math.max(t.minWidth || 0, ni), o && t.previousWidth + o < s ? (o += t.previousWidth - s, t.width = s) : (t.width = t.previousWidth + o, o = 0))
                    }
                    se();
                    e.syncColumnCellResize && rf()
                }).bind("dragend", function () {
                    var u;
                    for (n(this).parent().removeClass("slick-header-column-active"), i = 0; i < r.length; i++) t = f[i], u = n(r[i]).outerWidth(), t.previousWidth !== u && t.rerenderOnResize && ci();
                    gu(!0);
                    ft();
                    h(o.onColumnsResized, {})
                }))
            })
        }

        function nf(t) {
            var i = 0;
            return n.each(["borderTopWidth", "borderBottomWidth", "paddingTop", "paddingBottom"], function (n, r) {
                i += parseFloat(t.css(r)) || 0
            }), i
        }

        function ks() {
            var t, r = ["borderLeftWidth", "borderRightWidth", "paddingLeft", "paddingRight"],
                u = ["borderTopWidth", "borderBottomWidth", "paddingTop", "paddingBottom"], i;
            t = n("<div class='ui-state-default slick-header-column' style='visibility:hidden'>-<\/div>").appendTo(p);
            hi = pf = 0;
            t.css("box-sizing") != "border-box" && t.css("-moz-box-sizing") != "border-box" && t.css("-webkit-box-sizing") != "border-box" && (n.each(r, function (n, i) {
                hi += parseFloat(t.css(i)) || 0
            }), n.each(u, function (n, i) {
                pf += parseFloat(t.css(i)) || 0
            }));
            t.remove();
            i = n("<div class='slick-row' />").appendTo(tt);
            t = n("<div class='slick-cell' id='' style='visibility:hidden'>-<\/div>").appendTo(i);
            vu = yu = 0;
            t.css("box-sizing") != "border-box" && t.css("-moz-box-sizing") != "border-box" && t.css("-webkit-box-sizing") != "border-box" && (n.each(r, function (n, i) {
                vu += parseFloat(t.css(i)) || 0
            }), n.each(u, function (n, i) {
                yu += parseFloat(t.css(i)) || 0
            }));
            i.remove();
            ni = Math.max(hi, vu)
        }

        function ee() {
            var r, t, i;
            for (wi = n("<style type='text/css' rel='stylesheet' />").appendTo(n("head")), r = e.rowHeight - yu, t = ["." + nt + " .slick-header-column { left: 1000px; }", "." + nt + " .slick-top-panel { height:" + e.topPanelHeight + "px; }", "." + nt + " .slick-headerrow-columns { height:" + e.headerRowHeight + "px; }", "." + nt + " .slick-cell { height:" + r + "px; }", "." + nt + " .slick-row { height:" + e.rowHeight + "px; }"], i = 0; i < f.length; i++) t.push("." + nt + " .l" + i + " { }"), t.push("." + nt + " .r" + i + " { }");
            wi[0].styleSheet ? wi[0].styleSheet.cssText = t.join(" ") : wi[0].appendChild(document.createTextNode(t.join(" ")))
        }

        function ds(n) {
            var r, u, i, f, t, e;
            if (!bi) {
                for (r = document.styleSheets, t = 0; t < r.length; t++) if ((r[t].ownerNode || r[t].owningElement) == wi[0]) {
                    bi = r[t];
                    break
                }
                if (!bi) throw new Error("Cannot find stylesheet.");
                for (cu = [], lu = [], u = bi.cssRules || bi.rules, t = 0; t < u.length; t++) e = u[t].selectorText, (i = /\.l\d+/.exec(e)) ? (f = parseInt(i[0].substr(2, i[0].length - 2), 10), cu[f] = u[t]) : (i = /\.r\d+/.exec(e)) && (f = parseInt(i[0].substr(2, i[0].length - 2), 10), lu[f] = u[t])
            }
            return {left: cu[n], right: lu[n]}
        }

        function oe() {
            wi.remove();
            bi = null
        }

        function gs() {
            k().cancelCurrentEdit();
            h(o.onBeforeDestroy, {});
            for (var n = ti.length; n--;) te(ti[n]);
            e.enableColumnReorder && p.filter(":ui-sortable").sortable("destroy");
            as();
            y.unbind(".slickgrid");
            oe();
            tt.unbind("draginit dragstart dragend drag");
            y.empty().removeClass(nt)
        }

        function h(n, t, i) {
            return i = i || new Slick.EventData, t = t || {}, t.grid = o, n.notify(t, i, o)
        }

        function k() {
            return e.editorLock
        }

        function nh() {
            return vr
        }

        function kt(n) {
            return fr[n]
        }

        function tf() {
            for (var r, u = [], l = 0, i = 0, e, o = ki ? et - t.width : et, p, h, a, s, w, c, v, y, n = 0; n < f.length; n++) r = f[n], u.push(r.width), i += r.width, r.resizable && (l += r.width - Math.max(r.minWidth, ni));
            for (e = i; i > o && l;) {
                for (p = (i - o) / l, n = 0; n < f.length && i > o; n++) (r = f[n], h = u[n], !r.resizable || h <= r.minWidth || h <= ni) || (a = Math.max(r.minWidth, ni), s = Math.floor(p * (h - a)) || 1, s = Math.min(s, h - a), i -= s, l -= s, u[n] -= s);
                if (e <= i) break;
                e = i
            }
            for (e = i; i < o;) {
                for (w = o / i, n = 0; n < f.length && i < o; n++) r = f[n], c = u[n], v = !r.resizable || r.maxWidth <= c ? 0 : Math.min(Math.floor(w * c) - c, r.maxWidth - c || 1e6) || 1, i += v, u[n] += v;
                if (e >= i) break;
                e = i
            }
            for (y = !1, n = 0; n < f.length; n++) f[n].rerenderOnResize && f[n].width != u[n] && (y = !0), f[n].width = u[n];
            se();
            gu(!0);
            y && (ci(), ft())
        }

        function se() {
            var i;
            if (vt) {
                for (var t = 0, r = p.children(), u = r.length; t < u; t++) i = n(r[t]), i.width() !== f[t].width - hi && i.width(f[t].width - hi);
                uf()
            }
        }

        function rf() {
            for (var t = 0, r, i, n = 0; n < f.length; n++) r = f[n].width, i = ds(n), i.left.style.left = t + "px", i.right.style.right = pt - t - r + "px", t += f[n].width
        }

        function th(n, t) {
            kr([{columnId: n, sortAsc: t}])
        }

        function kr(t) {
            var i, r;
            for (rt = t, i = p.children(), i.removeClass("slick-header-column-sorted").attr("aria-sort", "none").find(".slick-sort-indicator").removeClass("slick-sort-indicator-asc slick-sort-indicator-desc"), r = 0; r < i.length; r++) headerColumnEl = n(i[r]), headerColumnEl.hasClass("slick-header-sortable") && headerColumnEl.attr("aria-label", headerColumnEl.text() + ": press enter to sort the table by this column");
            n.each(rt, function (n, t) {
                t.sortAsc == null && (t.sortAsc = !0);
                var r = kt(t.columnId);
                r != null && i.eq(r).addClass("slick-header-column-sorted").attr("aria-sort", t.sortAsc ? "ascending" : "descending").find(".slick-sort-indicator").addClass(t.sortAsc ? "slick-sort-indicator-asc" : "slick-sort-indicator-desc")
            })
        }

        function ih() {
            return rt
        }

        function he(n, t) {
            var u, r, i, s;
            for (wu = [], u = {}, r = 0; r < t.length; r++) for (i = t[r].fromRow; i <= t[r].toRow; i++) for (u[i] || (wu.push(i), u[i] = {}), s = t[r].fromCell; s <= t[r].toCell; s++) ko(i, s) && (u[i][f[s].id] = e.selectedCellCssClass);
            ge(e.selectedCellCssClass, u);
            h(o.onSelectedRowsChanged, {rows: ns()}, n)
        }

        function rh() {
            return f
        }

        function uf() {
            var t, n, i;
            for (nr = [], tr = [], t = 0, n = 0, i = f.length; n < i; n++) nr[n] = t, tr[n] = t + f[n].width, t += f[n].width
        }

        function ce(t) {
            var r, i;
            for (f = t, fr = {}, r = 0; r < f.length; r++) i = f[r] = n.extend({}, hu, f[r]), fr[i.id] = r, i.minWidth && i.width < i.minWidth && (i.width = i.minWidth), i.maxWidth && i.width > i.maxWidth && (i.width = i.maxWidth);
            uf();
            vt && (ci(), re(), oe(), ee(), gt(), rf(), hr())
        }

        function uh() {
            return e
        }

        function fh(t) {
            k().commitCurrentEdit() && (bt(), e.enableAddRow !== t.enableAddRow && we(ut()), e = n.extend(e, t), le(), g.css("overflow-y", e.autoHeight ? "hidden" : "auto"), ft())
        }

        function le() {
            e.autoHeight && (e.leaveSpaceForNewRows = !1)
        }

        function eh(n, t) {
            u = n;
            ci();
            iu();
            t && ri(0)
        }

        function oh() {
            return u
        }

        function ut() {
            return u.getLength ? u.getLength() : u.length
        }

        function dt() {
            return ut() + (e.enableAddRow ? 1 : 0)
        }

        function wt(n) {
            return u.getItem ? u.getItem(n) : u[n]
        }

        function sh() {
            return yf[0]
        }

        function hh(n) {
            e.showTopPanel != n && (e.showTopPanel = n, n ? oi.slideDown("fast", gt) : oi.slideUp("fast", gt))
        }

        function ch(n) {
            e.showHeaderRow != n && (e.showHeaderRow = n, n ? yt.slideDown("fast", gt) : yt.slideUp("fast", gt))
        }

        function lh() {
            return y.get(0)
        }

        function ff(n) {
            return e.rowHeight * n - d
        }

        function dr(n) {
            return Math.floor((n + d) / e.rowHeight)
        }

        function ri(n) {
            var r, i, u;
            n = Math.max(n, 0);
            n = Math.min(n, lt - b + (au ? t.height : 0));
            r = d;
            ui = Math.min(yi - 1, Math.floor(n / vi));
            d = Math.round(ui * lr);
            i = n - d;
            d != r && (u = ru(i), ye(u), gh());
            gi != i && (fi = gi + r < i + d ? 1 : -1, g[0].scrollTop = pr = w = gi = i, h(o.onViewportChanged, {}))
        }

        function ah(n, t, i) {
            return i == null ? "" : (i + "").replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;")
        }

        function gr(n, t) {
            var i = u.getItemMetadata && u.getItemMetadata(n),
                r = i && i.columns && (i.columns[t.id] || i.columns[kt(t.id)]);
            return r && r.formatter || i && i.formatter || t.formatter || e.formatterFactory && e.formatterFactory.getFormatter(t) || e.defaultFormatter
        }

        function ae(n, t) {
            var r = f[t], o = u.getItemMetadata && u.getItemMetadata(n), i = o && o.columns;
            return i && i[r.id] && i[r.id].editor !== undefined ? i[r.id].editor : i && i[t] && i[t].editor !== undefined ? i[t].editor : r.editor || e.editorFactory && e.editorFactory.getEditor(r)
        }

        function nu(n, t) {
            return e.dataItemColumnValueExtractor ? e.dataItemColumnValueExtractor(n, t) : n[t.field]
        }

        function vh(n, t, i, r) {
            var a = wt(t), w = t < r && !a,
                v = "slick-row" + (w ? " loading" : "") + (t === s ? " active" : "") + (t % 2 == 1 ? " odd" : " even"),
                h, c, p, o, l, y;
            for (a || (v += " " + e.addNewRowCssClass), h = u.getItemMetadata && u.getItemMetadata(t), h && h.cssClasses && (v += " " + h.cssClasses), n.push("<div class='ui-widget-content " + v + "' style='top:" + ff(t) + "px' role='row'>"), o = 0, l = f.length; o < l; o++) {
                if (p = f[o], c = 1, h && h.columns && (y = h.columns[p.id] || h.columns[o], c = y && y.colspan || 1, c === "*" && (c = l - o)), tr[Math.min(l - 1, o + c - 1)] > i.leftPx) {
                    if (nr[o] > i.rightPx) break;
                    ve(n, t, o, c, a)
                }
                c > 1 && (o += c - 1)
            }
            n.push("<\/div>")
        }

        function ve(n, t, i, r, u) {
            var o = f[i],
                c = "slick-cell l" + i + " r" + Math.min(f.length - 1, i + r - 1) + (o.cssClass ? " " + o.cssClass : ""),
                h, l, y, p, w, b;
            t === s && i === a && (c += " active");
            for (h in st) st[h][t] && st[h][t][o.id] && (c += " " + st[h][t][o.id]);
            l = -1;
            y = "gridcell";
            i === e.rowHeaderColumn && (l = 0, y = "rowheader");
            p = ("0" + t).slice(-2);
            w = ("0" + i).slice(-2);
            n.push("<div id='" + nt + p + w + "' class='" + c + "' tabindex='" + l + "' role='" + y + "'>");
            u && (b = nu(u, o), n.push(gr(t, o)(t, i, b, o, u)));
            n.push("<\/div>");
            v[t].cellRenderQueue.push(i);
            v[t].cellColSpans[i] = r
        }

        function ye(n) {
            for (var t in v) (t = parseInt(t, 10)) !== s && (t < n.top || t > n.bottom) && tu(t)
        }

        function yh() {
            iu();
            ci();
            ft()
        }

        function ci() {
            c && bt();
            for (var n in v) tu(n)
        }

        function tu(n) {
            var t = v[n];
            t && (br == t.rowNode ? (t.rowNode.style.display = "none", sr = br) : tt[0].removeChild(t.rowNode), delete v[n], delete ii[n], pu--, gf++)
        }

        function pe(n) {
            var t, i;
            if (n && n.length) for (fi = 0, t = 0, i = n.length; t < i; t++) c && s === n[t] && bt(), v[n[t]] && tu(n[t])
        }

        function we(n) {
            pe([n])
        }

        function ph(n, t) {
            var u = ht(n, t), r, i;
            u && (r = f[t], i = wt(n), c && s === n && a === t ? c.loadValue(i) : (u.innerHTML = i ? gr(n, r)(n, t, nu(i, r), r, i) : "", of(n)))
        }

        function ef(n) {
            var r = v[n], i, t, u, e;
            if (r) {
                uu(n);
                i = wt(n);
                for (t in r.cellNodesByColumnIdx) r.cellNodesByColumnIdx.hasOwnProperty(t) && (t = t | 0, u = f[t], e = r.cellNodesByColumnIdx[t], n === s && t === a && c ? c.loadValue(i) : e.innerHTML = i ? gr(n, u)(n, t, nu(i, u), u, i) : "");
                of(n)
            }
        }

        function wh() {
            return parseFloat(n.css(y[0], "height", !0)) - parseFloat(n.css(y[0], "paddingTop", !0)) - parseFloat(n.css(y[0], "paddingBottom", !0)) - parseFloat(n.css(pi[0], "height")) - nf(pi) - (e.showTopPanel ? e.topPanelHeight + nf(oi) : 0) - (e.showHeaderRow ? e.headerRowHeight + nf(yt) : 0)
        }

        function gt() {
            vt && (b = e.autoHeight ? e.rowHeight * dt() : wh(), yr = Math.ceil(b / e.rowHeight), et = parseFloat(n.css(y[0], "width", !0)), e.autoHeight || g.height(b), e.forceFitColumns && tf(), iu(), hr(), ur = -1, ft())
        }

        function iu() {
            var n, r, u, h;
            if (vt) {
                var f = dt(), o = f + (e.leaveSpaceForNewRows ? yr - 1 : 0), c = ki;
                ki = !e.autoHeight && o * e.rowHeight > b;
                bt();
                n = f - 1;
                for (r in v) r >= n && tu(r);
                l && s > n && cf();
                u = at;
                lt = Math.max(e.rowHeight * o, b - t.height);
                lt < i ? (at = vi = lt, yi = 1, lr = 0) : (at = i, vi = at / 100, yi = Math.floor(lt / vi), lr = (lt - at) / (yi - 1));
                at !== u && (tt.css("height", at), w = g[0].scrollTop);
                h = w + d <= lt - b;
                lt == 0 || w == 0 ? ui = d = 0 : h ? ri(w + d) : ri(lt - b);
                at != u && e.autoHeight && gt();
                e.forceFitColumns && c != ki && tf();
                gu(!1)
            }
        }

        function ru(n, t) {
            return n == null && (n = w), t == null && (t = it), {
                top: dr(n),
                bottom: dr(n + b) + 1,
                leftPx: t,
                rightPx: t + et
            }
        }

        function be(n, t) {
            var i = ru(n, t), u = Math.round(b / e.rowHeight), r = 3;
            return fi == -1 ? (i.top -= u, i.bottom += r) : fi == 1 ? (i.top -= r, i.bottom += u) : (i.top -= r, i.bottom += r), i.top = Math.max(0, i.top), i.bottom = Math.min(dt() - 1, i.bottom), i.leftPx -= et, i.rightPx += et, i.leftPx = Math.max(0, i.leftPx), i.rightPx = Math.min(pt, i.rightPx), i
        }

        function uu(n) {
            var t = v[n], i, r;
            if (t && t.cellRenderQueue.length) for (i = t.rowNode.lastChild; t.cellRenderQueue.length;) r = t.cellRenderQueue.pop(), t.cellNodesByColumnIdx[r] = i, i = i.previousSibling
        }

        function bh(n, t) {
            var h = 0, r = v[t], e = [], i, o, u;
            for (i in r.cellNodesByColumnIdx) r.cellNodesByColumnIdx.hasOwnProperty(i) && (i = i | 0, o = r.cellColSpans[i], (nr[i] > n.rightPx || tr[Math.min(f.length - 1, i + o - 1)] < n.leftPx) && (t == s && i == a || e.push(i)));
            while ((u = e.pop()) != null) r.rowNode.removeChild(r.cellNodesByColumnIdx[u]), delete r.cellColSpans[u], delete r.cellNodesByColumnIdx[u], ii[t] && delete ii[t][u], h++
        }

        function kh(n) {
            for (var o, w, t, h, l, a, b, y, k, e, c = [], p = [], s, d = 0, i, r = n.top, g = n.bottom; r <= g; r++) if (e = v[r], e) {
                for (uu(r), bh(n, r), s = 0, o = u.getItemMetadata && u.getItemMetadata(r), o = o && o.columns, w = wt(r), t = 0, h = f.length; t < h; t++) {
                    if (nr[t] > n.rightPx) break;
                    if ((i = e.cellColSpans[t]) != null) {
                        t += i > 1 ? i - 1 : 0;
                        continue
                    }
                    i = 1;
                    o && (l = o[f[t].id] || o[t], i = l && l.colspan || 1, i === "*" && (i = h - t));
                    tr[Math.min(h - 1, t + i - 1)] > n.leftPx && (ve(c, r, t, i, w), s++);
                    t += i > 1 ? i - 1 : 0
                }
                s && (d += s, p.push(r))
            }
            if (c.length) for (a = document.createElement("div"), a.innerHTML = c.join(""); (b = p.pop()) != null;) for (e = v[b]; (k = e.cellRenderQueue.pop()) != null;) y = a.lastChild, e.rowNode.appendChild(y), e.cellNodesByColumnIdx[k] = y
        }

        function dh(n) {
            for (var u, o = tt[0], f = [], i = [], e = !1, h = ut(), t = n.top, r = n.bottom; t <= r; t++) v[t] || (pu++, i.push(t), v[t] = {
                rowNode: null,
                cellColSpans: [],
                cellNodesByColumnIdx: [],
                cellRenderQueue: []
            }, vh(f, t, n, h), l && s === t && (e = !0), df++);
            if (i.length) {
                for (u = document.createElement("div"), u.innerHTML = f.join(""), t = 0, r = i.length; t < r; t++) v[i[t]].rowNode = o.appendChild(u.firstChild);
                e && (l = ht(s, a))
            }
        }

        function ke() {
            e.enableAsyncPostRender && (clearTimeout(ku), ku = setTimeout(de, e.asyncPostRenderDelay))
        }

        function of(n) {
            delete ii[n];
            or = Math.min(or, n);
            er = Math.max(er, n);
            ke()
        }

        function gh() {
            for (var n in v) v[n].rowNode.style.top = ff(n) + "px"
        }

        function ft() {
            if (vt) {
                var t = ru(), n = be();
                ye(n);
                ur != it && kh(n);
                dh(n);
                or = t.top;
                er = Math.min(dt() - 1, t.bottom);
                ke();
                pr = w;
                ur = it;
                wr = null
            }
        }

        function nc() {
            var n = yt[0].scrollLeft;
            n != g[0].scrollLeft && (g[0].scrollLeft = n)
        }

        function hr() {
            var n, t, i;
            w = g[0].scrollTop;
            it = g[0].scrollLeft;
            n = Math.abs(w - gi);
            t = Math.abs(it - kf);
            t && (kf = it, pi[0].scrollLeft = it, oi[0].scrollLeft = it, yt[0].scrollLeft = it);
            n && (fi = gi < w ? 1 : -1, gi = w, n < b ? ri(w + d) : (i = d, ui = at == b ? 0 : Math.min(yi - 1, Math.floor(w * ((lt - b) / (at - b)) * (1 / vi))), d = Math.round(ui * lr), i != d && ci()));
            (t || n) && (wr && clearTimeout(wr), (Math.abs(pr - w) > 20 || Math.abs(ur - it) > 20) && (e.forceSyncScrolling || Math.abs(pr - w) < b && Math.abs(ur - it) < et ? ft() : wr = setTimeout(ft, 50), h(o.onViewportChanged, {})));
            h(o.onScroll, {scrollLeft: it, scrollTop: w})
        }

        function de() {
            for (var o = ut(), n, i, t, r, u; or <= er;) if (n = fi >= 0 ? or++ : er--, i = v[n], i && !(n >= o)) {
                ii[n] || (ii[n] = {});
                uu(n);
                for (t in i.cellNodesByColumnIdx) i.cellNodesByColumnIdx.hasOwnProperty(t) && (t = t | 0, r = f[t], r.asyncPostRender && !ii[n][t] && (u = i.cellNodesByColumnIdx[t], u && r.asyncPostRender(u, n, wt(n), r), ii[n][t] = !0));
                ku = setTimeout(de, e.asyncPostRenderDelay);
                return
            }
        }

        function sf(t, i) {
            var e, r, u, f;
            for (var o in v) {
                if (f = i && i[o], u = t && t[o], f) for (r in f) u && f[r] == u[r] || (e = ht(o, kt(r)), e && n(e).removeClass(f[r]));
                if (u) for (r in u) f && f[r] == u[r] || (e = ht(o, kt(r)), e && n(e).addClass(u[r]))
            }
        }

        function tc(n, t) {
            if (st[n]) throw"addCellCssStyles: cell CSS hash with key '" + n + "' already exists.";
            st[n] = t;
            sf(t, null);
            h(o.onCellCssStylesChanged, {key: n, hash: t})
        }

        function ic(n) {
            st[n] && (sf(null, st[n]), delete st[n], h(o.onCellCssStylesChanged, {key: n, hash: null}))
        }

        function ge(n, t) {
            var i = st[n];
            st[n] = t;
            sf(t, i);
            h(o.onCellCssStylesChanged, {key: n, hash: t})
        }

        function rc(n) {
            return st[n]
        }

        function uc(t, i, r) {
            if (r = r || 100, v[t]) {
                var u = n(ht(t, i));

                function f(n) {
                    n && setTimeout(function () {
                        u.queue(function () {
                            u.toggleClass(e.cellFlashingCssClass).dequeue();
                            f(n - 1)
                        })
                    }, r)
                }

                f(4)
            }
        }

        function fc(t) {
            var i = n(t.target).closest(".slick-row")[0];
            i != br && (sr && sr != i && (tt[0].removeChild(sr), sr = null), br = i)
        }

        function ec(n, t) {
            var i = cr(n), r;
            return !i || !hf(i.row, i.cell) ? !1 : (r = h(o.onDragInit, t, n), n.isImmediatePropagationStopped()) ? r : !1
        }

        function oc(n, t) {
            var i = cr(n), r;
            return !i || !hf(i.row, i.cell) ? !1 : (r = h(o.onDragStart, t, n), n.isImmediatePropagationStopped()) ? r : !1
        }

        function sc(n, t) {
            return h(o.onDrag, t, n)
        }

        function hc(n, t) {
            h(o.onDragEnd, t, n)
        }

        function cc(n) {
            h(o.onKeyDown, {row: s, cell: a}, n);
            var t = n.isImmediatePropagationStopped();
            if (!t && !n.shiftKey && !n.altKey && !n.ctrlKey) if (n.which == 27) {
                if (!k().isActive()) return;
                eo()
            } else n.which == 34 ? (lo(), t = !0) : n.which == 33 ? (ao(), t = !0) : n.which == 37 ? t = wo() : n.which == 39 ? t = po() : n.which == 38 ? t = bo() : n.which == 40 ? t = su() : n.which == 13 && (e.editable ? c ? s === ut() ? su() : fo() : k().commitCurrentEdit() && eu() : to(n), t = !0);
            if (t) {
                n.stopPropagation();
                n.preventDefault();
                try {
                    n.originalEvent.keyCode = 0
                } catch (i) {
                }
            }
        }

        function lc(n) {
            h(o.onKeyDown, {row: s, cell: a}, n);
            var t = n.isImmediatePropagationStopped();
            if (t || n.shiftKey || n.altKey || n.ctrlKey || n.which != 13 || (ue(n), t = !0), t) {
                n.stopPropagation();
                n.preventDefault();
                try {
                    n.originalEvent.keyCode = 0
                } catch (i) {
                }
            }
        }

        function ac(n) {
            no(n, 1)
        }

        function vc(n) {
            no(n, 2)
        }

        function no(n, t) {
            h(o.onKeyDown, {row: s, cell: a}, n);
            var i = n.isImmediatePropagationStopped();
            if (i || n.which != 9 || n.ctrlKey || n.altKey || (t != 1 || n.shiftKey ? t == 2 && n.shiftKey && (rr.focus(), i = !0) : (ar.focus(), i = !0)), i) {
                n.stopPropagation();
                n.preventDefault();
                try {
                    n.originalEvent.keyCode = 0
                } catch (r) {
                }
            }
        }

        function to(n) {
            var t = cr(n);
            t && (c === null || s != t.row || a != t.cell) && ((h(o.onClick, {
                row: t.row,
                cell: t.cell
            }, n), n.isImmediatePropagationStopped()) || (a != t.cell || s != t.row) && ct(t.row, t.cell) && (!k().isActive() || k().commitCurrentEdit()) && (lf(t.row, !1), li(ht(t.row, t.cell))))
        }

        function yc(t) {
            var i = n(t.target).closest(".slick-cell", tt);
            i.length !== 0 && (l !== i[0] || c === null) && h(o.onContextMenu, {}, t)
        }

        function pc(n) {
            var t = cr(n);
            t && (c === null || s != t.row || a != t.cell) && ((h(o.onDblClick, {
                row: t.row,
                cell: t.cell
            }, n), n.isImmediatePropagationStopped()) || e.editable && go(t.row, t.cell, !0))
        }

        function wc(t) {
            h(o.onHeaderMouseEnter, {column: n(this).data("column")}, t)
        }

        function bc(t) {
            h(o.onHeaderMouseLeave, {column: n(this).data("column")}, t)
        }

        function kc(t) {
            var i = n(t.target).closest(".slick-header-column", ".slick-header-columns"), r = i && i.data("column");
            h(o.onHeaderContextMenu, {column: r}, t)
        }

        function dc(t) {
            var i = n(t.target).closest(".slick-header-column", ".slick-header-columns"), r = i && i.data("column");
            r && h(o.onHeaderClick, {column: r}, t)
        }

        function gc(n) {
            h(o.onMouseEnter, {}, n)
        }

        function nl(n) {
            h(o.onMouseLeave, {}, n)
        }

        function hf(n, t) {
            return !(n < 0 || n >= ut() || t < 0 || t >= f.length)
        }

        function tl(n, t) {
            for (var e = dr(t), i = 0, u = 0, r = 0; r < f.length && u < n; r++) u += f[r].width, i++;
            return i < 0 && (i = 0), {row: e, cell: i - 1}
        }

        function io(n) {
            var t = /l\d+/.exec(n.className);
            if (!t) throw"getCellFromNode: cannot get cell - " + n.className;
            return parseInt(t[0].substr(1, t[0].length - 1), 10)
        }

        function ro(n) {
            for (var t in v) if (v[t].rowNode === n) return t | 0;
            return null
        }

        function cr(t) {
            var i = n(t.target).closest(".slick-cell", tt), r, u;
            return i.length ? (r = ro(i[0].parentNode), u = io(i[0]), r == null || u == null ? null : {
                row: r,
                cell: u
            }) : null
        }

        function il(n, t) {
            var i, o;
            if (!hf(n, t)) return null;
            var u = ff(n), s = u + e.rowHeight - 1, r = 0;
            for (i = 0; i < t; i++) r += f[i].width;
            return o = r + f[t].width, {top: u, left: r, bottom: s, right: o}
        }

        function cf() {
            li(null, !1)
        }

        function rl() {
            wf == -1 ? rr[0].focus() : ar[0].focus()
        }

        function fu(n, t, i) {
            lf(n, i);
            var u = ai(n, t), r = nr[t], f = tr[t + (u > 1 ? u - 1 : 0)], e = it + et;
            r < it ? (g.scrollLeft(r), hr(), ft()) : f > e && (g.scrollLeft(Math.min(r, f - g[0].clientWidth)), hr(), ft())
        }

        function li(t, i) {
            l !== null && (bt(), n(l).removeClass("active"), v[s] && n(v[s].rowNode).removeClass("active"));
            var r = l !== t;
            l = t;
            l != null ? (s = ro(l.parentNode), a = di = io(l), i == null && (i = s == ut() || e.autoEdit), n(l).addClass("active"), n(v[s].rowNode).addClass("active"), e.editable && i && uo(s, a) && (clearTimeout(bu), e.asyncEditorLoading ? bu = setTimeout(function () {
                eu()
            }, e.asyncEditorLoadDelay) : eu())) : s = a = null;
            r && (l && l.focus(), h(o.onActiveCellChanged, ho()))
        }

        function ul() {
            if (document.selection && document.selection.empty) try {
                document.selection.empty()
            } catch (t) {
            } else if (window.getSelection) {
                var n = window.getSelection();
                n && n.removeAllRanges && n.removeAllRanges()
            }
        }

        function uo(n, t) {
            var i = ut();
            return n < i && !wt(n) ? !1 : f[t].cannotTriggerInsert && n >= i ? !1 : ae(n, t) ? !0 : !1
        }

        function bt() {
            var t, i, r;
            c && (h(o.onBeforeCellEditorDestroy, {editor: c}), c.destroy(), c = null, l && (t = wt(s), n(l).removeClass("editable invalid"), t && (i = f[a], r = gr(s, i), l.innerHTML = r(s, a, nu(t, i), i, t), of(s))), navigator.userAgent.toLowerCase().match(/msie/) && ul(), k().deactivate(vr))
        }

        function eu(t) {
            if (l) {
                if (!e.editable) throw"Grid : makeActiveCellEditable : should never get called when options.editable is false";
                if (clearTimeout(bu), uo(s, a)) {
                    var r = f[a], i = wt(s);
                    h(o.onBeforeEditCell, {
                        row: s,
                        cell: a,
                        item: i,
                        column: r
                    }) !== !1 && (k().activate(vr), n(l).addClass("editable"), t || (l.innerHTML = ""), c = new (t || ae(s, a))({
                        grid: o,
                        gridPosition: ou(y[0]),
                        position: ou(l),
                        container: l,
                        column: r,
                        item: i || {},
                        commitChanges: fo,
                        cancelChanges: eo
                    }), i && c.loadValue(i), bf = c.serializeValue(), c.position && so())
                }
            }
        }

        function fo() {
            k().commitCurrentEdit() && e.autoEdit && su()
        }

        function eo() {
            k().cancelCurrentEdit()
        }

        function ou(t) {
            var i = {
                top: t.offsetTop,
                left: t.offsetLeft,
                bottom: 0,
                right: 0,
                width: n(t).outerWidth(),
                height: n(t).outerHeight(),
                visible: !0
            }, r;
            for (i.bottom = i.top + i.height, i.right = i.left + i.width, r = t.offsetParent; (t = t.parentNode) != document.body;) i.visible && t.scrollHeight != t.offsetHeight && n(t).css("overflowY") != "visible" && (i.visible = i.bottom > t.scrollTop && i.top < t.scrollTop + t.clientHeight), i.visible && t.scrollWidth != t.offsetWidth && n(t).css("overflowX") != "visible" && (i.visible = i.right > t.scrollLeft && i.left < t.scrollLeft + t.clientWidth), i.left -= t.scrollLeft, i.top -= t.scrollTop, t === r && (i.left += t.offsetLeft, i.top += t.offsetTop, r = t.offsetParent), i.bottom = i.top + i.height, i.right = i.left + i.width;
            return i
        }

        function oo() {
            return ou(l)
        }

        function fl() {
            return ou(y[0])
        }

        function so() {
            if (l && (h(o.onActiveCellPositionChanged, {}), c)) {
                var n = oo();
                c.show && c.hide && (n.visible ? c.show() : c.hide());
                c.position && c.position(n)
            }
        }

        function el() {
            return c
        }

        function ho() {
            return l ? {row: s, cell: a} : null
        }

        function ol() {
            return l
        }

        function lf(n, i) {
            var r = n * e.rowHeight, u = (n + 1) * e.rowHeight - b + (au ? t.height : 0);
            (n + 1) * e.rowHeight > w + b + d ? (ri(i ? r : u), ft()) : n * e.rowHeight < w + d && (ri(i ? u : r), ft())
        }

        function sl(n) {
            ri(n * e.rowHeight);
            ft()
        }

        function co(n) {
            var f = n * yr, t, r;
            if (ri((dr(w) + f) * e.rowHeight), ft(), e.enableCellNavigation && s != null) {
                t = s + f;
                r = dt();
                t >= r && (t = r - 1);
                t < 0 && (t = 0);
                for (var i = 0, u = null, o = di; i <= di;) ct(t, i) && (u = i), i += ai(t, i);
                u !== null ? (li(ht(t, u)), di = o) : cf()
            }
        }

        function lo() {
            co(1)
        }

        function ao() {
            co(-1)
        }

        function ai(n, t) {
            var i = u.getItemMetadata && u.getItemMetadata(n), r, e;
            return !i || !i.columns ? 1 : (r = i.columns[f[t].id] || i.columns[t], e = r && r.colspan, e === "*" ? f.length - t : e || 1)
        }

        function vo(n) {
            for (var t = 0; t < f.length;) {
                if (ct(n, t)) return t;
                t += ai(n, t)
            }
            return null
        }

        function hl(n) {
            for (var t = 0, i = null; t < f.length;) ct(n, t) && (i = t), t += ai(n, t);
            return i
        }

        function af(n, t) {
            if (t >= f.length) return null;
            do t += ai(n, t); while (t < f.length && !ct(n, t));
            return t < f.length ? {row: n, cell: t, posX: t} : null
        }

        function yo(n, t) {
            var r, i, u;
            if (t <= 0 || (r = vo(n), r === null || r >= t)) return null;
            for (i = {row: n, cell: r, posX: r}; ;) {
                if (u = af(i.row, i.cell, i.posX), !u) return null;
                if (u.cell >= t) return i;
                i = u
            }
        }

        function cl(n, t, i) {
            for (var r, u = dt(); ;) {
                if (++n >= u) return null;
                for (r = t = 0; t <= i;) r = t, t += ai(n, t);
                if (ct(n, r)) return {row: n, cell: r, posX: i}
            }
        }

        function ll(n, t, i) {
            for (var r; ;) {
                if (--n < 0) return null;
                for (r = t = 0; t <= i;) r = t, t += ai(n, t);
                if (ct(n, r)) return {row: n, cell: r, posX: i}
            }
        }

        function al(n, t, i) {
            var u, r, f;
            if (n == null && t == null && (n = t = i = 0, ct(n, t))) return {row: n, cell: t, posX: t};
            if (u = af(n, t, i), u) return u;
            for (r = null, f = dt(); ++n < f;) if (r = vo(n), r !== null) return {row: n, cell: r, posX: r};
            return null
        }

        function vl(n, t, i) {
            if (n == null && t == null && (n = dt() - 1, t = i = f.length - 1, ct(n, t))) return {
                row: n,
                cell: t,
                posX: t
            };
            for (var r, u; !r;) {
                if (r = yo(n, t, i), r) break;
                if (--n < 0) return null;
                t = 0;
                u = hl(n);
                u !== null && (r = {row: n, cell: u, posX: u})
            }
            return r
        }

        function po() {
            return ir("right")
        }

        function wo() {
            return ir("left")
        }

        function su() {
            return ir("down")
        }

        function bo() {
            return ir("up")
        }

        function yl() {
            return ir("next")
        }

        function pl() {
            return ir("prev")
        }

        function ir(n) {
            var i, r;
            if (!e.enableCellNavigation || !l && n != "prev" && n != "next") return !1;
            if (!k().commitCurrentEdit()) return !0;
            i = {up: -1, down: 1, left: -1, right: 1, prev: -1, next: 1};
            wf = i[n];
            var u = {up: ll, down: cl, left: yo, right: af, prev: vl, next: al}, f = u[n], t = f(s, a, di);
            return t ? (r = t.row == ut(), fu(t.row, t.cell, !r), li(ht(t.row, t.cell)), di = t.posX, !0) : (li(ht(s, a)), !1)
        }

        function ht(n, t) {
            return v[n] ? (uu(n), v[n].cellNodesByColumnIdx[t]) : null
        }

        function wl(n, t) {
            vt && (n > ut() || n < 0 || t >= f.length || t < 0 || e.enableCellNavigation && (fu(n, t, !1), li(ht(n, t), !1)))
        }

        function ct(n, t) {
            var r, i;
            return !e.enableCellNavigation || n >= dt() || n < 0 || t >= f.length || t < 0 ? !1 : (r = u.getItemMetadata && u.getItemMetadata(n), r && typeof r.focusable == "boolean") ? r.focusable : (i = r && r.columns, i && i[f[t].id] && typeof i[f[t].id].focusable == "boolean") ? i[f[t].id].focusable : i && i[t] && typeof i[t].focusable == "boolean" ? i[t].focusable : f[t].focusable
        }

        function ko(n, t) {
            var i, r;
            return n >= ut() || n < 0 || t >= f.length || t < 0 ? !1 : (i = u.getItemMetadata && u.getItemMetadata(n), i && typeof i.selectable == "boolean") ? i.selectable : (r = i && i.columns && (i.columns[f[t].id] || i.columns[t]), r && typeof r.selectable == "boolean") ? r.selectable : f[t].selectable
        }

        function go(n, t, i) {
            if (vt && ct(n, t) && k().commitCurrentEdit()) {
                fu(n, t, !1);
                var r = ht(n, t);
                li(r, i || n === ut() || e.autoEdit)
            }
        }

        function bl() {
            var t = wt(s), i = f[a], r, u, v;
            if (c) {
                if (c.isValueChanged()) return r = c.validate(), r.valid ? (s < ut() ? (u = {
                    row: s,
                    cell: a,
                    editor: c,
                    serializedValue: c.serializeValue(),
                    prevSerializedValue: bf,
                    execute: function () {
                        this.editor.applyValue(t, this.serializedValue);
                        ef(this.row);
                        h(o.onCellChange, {row: s, cell: a, item: t})
                    },
                    undo: function () {
                        this.editor.applyValue(t, this.prevSerializedValue);
                        ef(this.row);
                        h(o.onCellChange, {row: s, cell: a, item: t})
                    }
                }, e.editCommandHandler ? (bt(), e.editCommandHandler(t, i, u)) : (u.execute(), bt())) : (v = {}, c.applyValue(v, c.serializeValue()), bt(), h(o.onAddNewRow, {
                    item: v,
                    column: i
                })), !k().isActive()) : (n(l).removeClass("invalid"), n(l).width(), n(l).addClass("invalid"), h(o.onValidationError, {
                    editor: c,
                    cellNode: l,
                    validationResults: r,
                    row: s,
                    cell: a,
                    column: i
                }), c.focus(), !1);
                bt()
            }
            return !0
        }

        function kl() {
            return bt(), !0
        }

        function dl(n) {
            for (var i = [], r = f.length - 1, t = 0; t < n.length; t++) i.push(new Slick.Range(n[t], 0, n[t], r));
            return i
        }

        function ns() {
            if (!ot) throw"Selection model is not set";
            return wu
        }

        function gl(n) {
            if (!ot) throw"Selection model is not set";
            ot.setSelectedRanges(dl(n))
        }

        var ts = {
                explicitInitialization: !1,
                rowHeight: 25,
                defaultColumnWidth: 80,
                enableAddRow: !1,
                leaveSpaceForNewRows: !1,
                editable: !1,
                autoEdit: !0,
                enableCellNavigation: !0,
                enableColumnReorder: !0,
                asyncEditorLoading: !1,
                asyncEditorLoadDelay: 100,
                forceFitColumns: !1,
                enableAsyncPostRender: !1,
                asyncPostRenderDelay: 50,
                autoHeight: !1,
                editorLock: Slick.GlobalEditorLock,
                showHeaderRow: !1,
                headerRowHeight: 25,
                showTopPanel: !1,
                topPanelHeight: 25,
                formatterFactory: null,
                editorFactory: null,
                cellFlashingCssClass: "flashing",
                selectedCellCssClass: "selected",
                multiSelect: !0,
                enableTextSelectionOnCells: !1,
                dataItemColumnValueExtractor: null,
                fullWidthRows: !1,
                multiColumnSort: !1,
                defaultFormatter: ah,
                forceSyncScrolling: !1,
                addNewRowCssClass: "new-row"
            }, hu = {
                name: "",
                resizable: !0,
                sortable: !1,
                minWidth: 30,
                rerenderOnResize: !1,
                headerCssClass: null,
                defaultSortAsc: !0,
                focusable: !0,
                selectable: !0
            }, lt, at, vi, yi, lr, ui = 0, d = 0, fi = 1, vt = !1, y, nt = "slickgrid_" + Math.round(1e6 * Math.random()),
            o = this, rr, ar, pi, p, ei, yt, vf, oi, yf, g, tt, wi, si, bi, cu, lu, b, et, pt, au, ki, hi = 0, pf = 0,
            vu = 0, yu = 0, ni, wf = 1, di, s, a, l = null, c = null, bf, vr, v = {}, pu = 0, yr, gi = 0, w = 0, pr = 0,
            ur = 0, kf = 0, it = 0, ot, wu = [], ti = [], st = {}, fr = {}, rt = [], nr = [], tr = [], bu = null,
            wr = null, ku = null, ii = {}, er = null, or = null, df = 0, gf = 0, br, sr;
        this.debug = function () {
            var n = "";
            n += "\ncounter_rows_rendered:  " + df;
            n += "\ncounter_rows_removed:  " + gf;
            n += "\nrenderedRows:  " + pu;
            n += "\nnumVisibleRows:  " + yr;
            n += "\nmaxSupportedCssHeight:  " + i;
            n += "\nn(umber of pages):  " + yi;
            n += "\n(current) page:  " + ui;
            n += "\npage height (ph):  " + vi;
            n += "\nvScrollDir:  " + fi;
            alert(n)
        };
        this.eval = function (n) {
            return eval(n)
        };
        n.extend(this, {
            slickGridVersion: "2.1",
            onScroll: new Slick.Event,
            onSort: new Slick.Event,
            onHeaderMouseEnter: new Slick.Event,
            onHeaderMouseLeave: new Slick.Event,
            onHeaderContextMenu: new Slick.Event,
            onHeaderClick: new Slick.Event,
            onHeaderCellRendered: new Slick.Event,
            onBeforeHeaderCellDestroy: new Slick.Event,
            onHeaderRowCellRendered: new Slick.Event,
            onBeforeHeaderRowCellDestroy: new Slick.Event,
            onMouseEnter: new Slick.Event,
            onMouseLeave: new Slick.Event,
            onClick: new Slick.Event,
            onDblClick: new Slick.Event,
            onContextMenu: new Slick.Event,
            onKeyDown: new Slick.Event,
            onAddNewRow: new Slick.Event,
            onValidationError: new Slick.Event,
            onViewportChanged: new Slick.Event,
            onColumnsReordered: new Slick.Event,
            onColumnsResized: new Slick.Event,
            onCellChange: new Slick.Event,
            onBeforeEditCell: new Slick.Event,
            onBeforeCellEditorDestroy: new Slick.Event,
            onBeforeDestroy: new Slick.Event,
            onActiveCellChanged: new Slick.Event,
            onActiveCellPositionChanged: new Slick.Event,
            onDragInit: new Slick.Event,
            onDragStart: new Slick.Event,
            onDrag: new Slick.Event,
            onDragEnd: new Slick.Event,
            onSelectedRowsChanged: new Slick.Event,
            onCellCssStylesChanged: new Slick.Event,
            registerPlugin: us,
            unregisterPlugin: te,
            getColumns: rh,
            setColumns: ce,
            getColumnIndex: kt,
            updateColumnHeader: vs,
            setSortColumn: th,
            setSortColumns: kr,
            getSortColumns: ih,
            autosizeColumns: tf,
            getOptions: uh,
            setOptions: fh,
            getData: oh,
            getDataLength: ut,
            getDataItem: wt,
            setData: eh,
            getSelectionModel: es,
            setSelectionModel: fs,
            getSelectedRows: ns,
            setSelectedRows: gl,
            getContainerNode: lh,
            render: ft,
            invalidate: yh,
            invalidateRow: we,
            invalidateRows: pe,
            invalidateAllRows: ci,
            updateCell: ph,
            updateRow: ef,
            getViewport: ru,
            getRenderedRange: be,
            resizeCanvas: gt,
            updateRowCount: iu,
            scrollRowIntoView: lf,
            scrollRowToTop: sl,
            scrollCellIntoView: fu,
            getCanvasNode: os,
            focus: rl,
            getCellFromPoint: tl,
            getCellFromEvent: cr,
            getActiveCell: ho,
            setActiveCell: wl,
            getActiveCellNode: ol,
            getActiveCellPosition: oo,
            resetActiveCell: cf,
            editActiveCell: eu,
            getCellEditor: el,
            getCellNode: ht,
            getCellNodeBox: il,
            canCellBeSelected: ko,
            canCellBeActive: ct,
            navigatePrev: pl,
            navigateNext: yl,
            navigateUp: bo,
            navigateDown: su,
            navigateLeft: wo,
            navigateRight: po,
            navigatePageUp: ao,
            navigatePageDown: lo,
            gotoCell: go,
            getTopPanel: sh,
            setTopPanelVisibility: hh,
            setHeaderRowVisibility: ch,
            getHeaderRow: ys,
            getHeaderRowColumn: ps,
            getGridPosition: fl,
            flashCell: uc,
            addCellCssStyles: tc,
            setCellCssStyles: ge,
            removeCellCssStyles: ic,
            getCellCssStyles: rc,
            init: ne,
            destroy: gs,
            getEditorLock: k,
            getEditController: nh
        });
        is()
    }

    n.extend(!0, window, {Slick: {Grid: r}});
    var t, i
})(jQuery)