﻿function KernelModeDumpManager(){function i(){var i=n.find("option:selected"),r;i.length>0?(t.show(),r={filename:i.val()},t.attr("href","api/debug/dump/kernel/dump?"+$.param(r))):t.hide()}var r=new WebbRest,n=$("#bugcheckDumpsList"),t=$("#bugcheckFileDownloadLink");t.hide();n.change(function(){i()});r.getBugcheckDumpsList().done(function(t){for(var r in t.DumpFiles)n.append($("<option>",{value:t.DumpFiles[r].FileName,text:t.DumpFiles[r].FileName}));t.DumpFiles.length>0&&(n.val(t.DumpFiles[0].FileName),i())})}var WebbRest;$(function(){KernelModeDumpManager()});
/*!
//@ sourceURL=tools/LiveKernelDumps/LiveKernelDumps.js
*/
